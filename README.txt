Image_QUality è un plug-in di ImageJ ed è stato sviluppato con lo scopo di facilitare ed automatizzare 
l'analisi della qualità delle immagini biomediche, in particolare delle Risonanze Magnetiche 

Per poter configurare il plugin su Eclipse si segua questa guida (il primo metodo): 
https://imagejdocu.tudor.lu/howto/plugins/the_imagej_eclipse_howto
quando si arriva al momento di di dover creare il progetto del plugin lo si chiami Image_Quality, si copi 
la cartella src contenuta nel repository nella cartella src del progetto Image_Quality appena creato e infine
si copi il file build.xml nella cartella del plug-in. Si finisca la configutrazione come descritto e si 
dovrebbe essere in grado di far partire correttamente il plug-in.
Se il programma su Eclipse non funziona correttamenta potrebbe essere un problema del file build.xml, lo si 
elimini e lo di rigeniri come mostrato sulla guida inserendo al posto di TestPlugin_ Image_Quality.

Nel repository nella cartella resources è presente un'immagine con cui poter testare il plug in  