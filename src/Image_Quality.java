import gui.view.MyGui;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

/**
 * Is the class that implements PlugInFilter and run the plug-in.
 * It must be in the default package 
 * 
 */
public class Image_Quality implements PlugInFilter {

	/**
	 *{@inheritDoc}
	 */
	@Override
	public void run(ImageProcessor arg0) {
		new MyGui(arg0);
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public int setup(String arg0, ImagePlus arg1) {
		return DOES_ALL;
	}
}
