package gui.logic;

import java.awt.Rectangle;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import gui.view.MyGui;
import ij.ImagePlus;

/**
 * Disable, enable or modify the GUI's component depending on the changes in the overlay
 *
 */
public class RoiObserver extends Thread {
	
	private final List<JButton> buttons;
	private final ImagePlus image;
	private final JComboBox<String> selector;
	private int lastCout = 0;
	
	/**
	 * @param image ImagePlus with the Overlay to observe
	 * @param selector JComboBox containing the possible ROI to choose
	 * @param buttons Variable array of buttons that have to be disable if there is no ROI in the image
	 */
	public RoiObserver(final ImagePlus image, final JComboBox<String> selector, final JButton... buttons) {
		this.buttons = Arrays.asList(buttons);
		this.image = image;
		this.selector = selector;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public void run() {
		while(true) {
			if(this.image.getOverlay().size() == 0) {
				this.buttons.stream().forEach(i -> i.setEnabled(false));
				this.selector.setEnabled(false);
				this.selector.removeAllItems();
				this.lastCout = 0;
			} else {
				this.buttons.stream().forEach(i -> i.setEnabled(true));
				this.selector.setEnabled(true);
				if(this.lastCout  != this.image.getOverlay().size()) {
					this.selector.removeAllItems();
					for(int i=0; i<(this.lastCout = this.image.getOverlay().size()); i++) {
						this.selector.addItem("ROI " + (i+1));
					}
					this.selector.setSelectedIndex(this.lastCout - 1);
					Rectangle rec = this.image.getOverlay().get(selector.getSelectedIndex()).getBounds();
		        	MyGui.displaySize((int) rec.getWidth(), (int) rec.getHeight());	
				}
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
