package gui.logic;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import ij.gui.OvalRoi;
import ij.gui.Roi;

/**
 * A class with only static useful methods for creation of ROI 
 *
 */
public class RoiGenerators {
	
	/**
	 * private generator to avoid initialization 
	 */
	private RoiGenerators() {
	}
	
	/**
	 * @param firstClik point where the ROI start
	 * @param releasePoint point where the ROI end
	 * @param roiShape the shape of the ROI
	 * @return new ROI 
	 */
	public static Roi generateRoi(final Point firstClik, final Point releasePoint, final Shape roiShape) {
		List<Double> param = getShapeParameter(firstClik, releasePoint, roiShape);
		if(roiShape.equals(Shape.RETANGLE)) {
			return new Roi(param.get(0), param.get(1), param.get(2), param.get(3));
		} else {
			return new OvalRoi(param.get(0), param.get(1), param.get(2), param.get(3));
		}
	}

	/**
	 * @param firstClik point where the ROI start
	 * @param releasePoint point where the ROI end
	 * @param roiShape the shape of the ROI
	 * @return a list containing respectively X,Y values of the left up corner of the ROI, the width and the height  
	 */
	public static List<Double> getShapeParameter(final Point firstClik, final Point releasePoint, final Shape roiShape) {
		final double height = releasePoint.getY() > firstClik.getY() ? releasePoint.getY() - firstClik.getY() : firstClik.getY() - releasePoint.getY();
		final double width = releasePoint.getX() > firstClik.getX() ?  releasePoint.getX() - firstClik.getX() : firstClik.getX() - releasePoint.getX();
		if(releasePoint.getY() > firstClik.getY() && releasePoint.getX() > firstClik.getX()) {
			if(roiShape.equals(Shape.RETANGLE)) {
				return Arrays.asList(firstClik.getX(), firstClik.getY(), width, height);
			} else {
				return Arrays.asList(firstClik.getX(), firstClik.getY(), width, height);
			}
		} else if(releasePoint.getY() < firstClik.getY() && releasePoint.getX() > firstClik.getX()) {
			if(roiShape.equals(Shape.RETANGLE)) {
				return Arrays.asList(firstClik.getX(), firstClik.getY() - height, width, height);
			} else {
				return Arrays.asList(firstClik.getX(), firstClik.getY() - height, width, height);
			}
		} else if(releasePoint.getY() < firstClik.getY() && releasePoint.getX() < firstClik.getX()) {
			if(roiShape.equals(Shape.RETANGLE)) {
				return Arrays.asList(releasePoint.getX(), releasePoint.getY() , width, height);
			} else {
				return Arrays.asList(releasePoint.getX(), releasePoint.getY() , width, height);
			}
		} else {
			if(roiShape.equals(Shape.RETANGLE)) {
				return Arrays.asList(releasePoint.getX(), releasePoint.getY() - height, width, height);
			} else {
				return Arrays.asList(releasePoint.getX(), releasePoint.getY() - height, width, height);
			}
		}
	}
}
