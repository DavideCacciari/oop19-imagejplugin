package gui.logic;

/**
 * Enumeration of possible ROI's shape 
 *
 */
public enum Shape {
	RETANGLE, OVAL;
	
	/**
	 * @return String representation of the Shape
	 */
	public String toString() {
		if(this.equals(RETANGLE)) {
			return "Retangle";
		} else {
			return "Oval";
		}
	}
}
