package gui.logic.roibuttons;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import gui.logic.RoiObserver;
import gui.view.MyGui;
import ij.gui.ImageCanvas;

/**
 *  Key listener for moving the ROI
 *
 */
public class MoveKeybordListener implements KeyListener {

	private boolean isMoving = false;
	private RoiObserver buttonAgent;
	private RoiManager roiManager;
	private ImageCanvas image;

	/**
	 * @param image Reference Image
	 * @param buttonAgent the agent that have to be resumed when the movement is over
	 * @param roiManager 
	 */
	public MoveKeybordListener(ImageCanvas image, RoiObserver buttonAgent, RoiManager roiManager) {
		this.buttonAgent = buttonAgent;
		this.roiManager = roiManager;
		this.image = image;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@SuppressWarnings("deprecation")
	@Override
	public void keyPressed(KeyEvent e) {
		if(this.isMoving) {
			if(e.getKeyCode() == 10) {
				System.out.println("entrato");
				this.isMoving = false;
				this.buttonAgent.resume();
				MyGui.disableWhileMoving(false);
			} else if(e.getKeyCode() == 37) {
				roiManager.move(MyGui.getSelectedRoi(), p -> new Point((int) p.getX() - 1 , (int) p.getY()));
			} else if(e.getKeyCode() == 39) {
				roiManager.move(MyGui.getSelectedRoi(), p -> new Point((int) p.getX() + 1 , (int) p.getY()));
			} else if(e.getKeyCode() == 38) {
				roiManager.move(MyGui.getSelectedRoi(), p -> new Point((int) p.getX(), (int) p.getY() - 1));
			} else if(e.getKeyCode() == 40) {
				roiManager.move(MyGui.getSelectedRoi(), p -> new Point((int) p.getX(), (int) p.getY() + 1));
			}
			image.repaintOverlay();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
	
	/**
	 * Method for internal use, don't call
	 */
	public void setMoving(boolean b) {
		this.isMoving  = b;
	}
}
