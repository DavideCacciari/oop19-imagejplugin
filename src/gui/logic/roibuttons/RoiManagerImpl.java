package gui.logic.roibuttons;

import java.awt.Point;
import java.util.function.Function;

import ij.ImagePlus;
import ij.gui.OvalRoi;
import ij.gui.Roi;

public class RoiManagerImpl implements RoiManager {
	
	private ImagePlus image;
	
	/**
	 * @param image the reference image
	 */
	public RoiManagerImpl(final ImagePlus image) {
		this.image = image;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(final int index) {
		image.getOverlay().remove(index);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void changeDimension(final int index, final int width, final int height) {
		Roi oldRoi = image.getOverlay().get(index);
		Point p = oldRoi.getBounds().getLocation();
		if(oldRoi.getClass().equals(Roi.class)) {
			image.getOverlay().set(new Roi(p.getX(), p.getY(), width, height), index);
		} else {
			image.getOverlay().set(new OvalRoi(p.getX(), p.getY(), width, height), index);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void move(int index, Function<Point, Point> getPoint) {
		Roi oldRoi = image.getOverlay().get(index);
		Point p = oldRoi.getBounds().getLocation();
		p.setLocation(getPoint.apply(p));
		if(oldRoi.getClass().equals(Roi.class)) {
			image.getOverlay().set(new Roi(p.getX(), p.getY(), oldRoi.getFloatWidth(), oldRoi.getFloatHeight()), index);
		} else {
			image.getOverlay().set(new OvalRoi(p.getX(), p.getY(), oldRoi.getFloatWidth(), oldRoi.getFloatHeight()), index);
		}
	}
}
