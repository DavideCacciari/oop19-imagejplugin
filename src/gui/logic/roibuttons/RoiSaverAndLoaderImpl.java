package gui.logic.roibuttons;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import javax.swing.JOptionPane;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.ImageCanvas;
import ij.gui.OvalRoi;

public class RoiSaverAndLoaderImpl implements RoiSaverAndLoader {
	
	private ImageCanvas image;
	private boolean exitLoader = false; 
	
	/**
	 * @param image the reference image
	 */
	public RoiSaverAndLoaderImpl(ImageCanvas image) {
		this.image = image;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void save(File file) throws IOException {
		if(!file.getAbsolutePath().contains(".json")) {
			String path = file.getAbsolutePath() + ".json";
			file = new File(path);
		}
		try (
		    OutputStream streamFile = new FileOutputStream(file);
		    BufferedOutputStream bstream = new BufferedOutputStream(streamFile);
			ObjectOutputStream ostream = new ObjectOutputStream(bstream);
		){
			for(int i=0; i<this.image.getOverlay().size(); i++) {
				ostream.writeBoolean(this.image.getOverlay().get(i).getClass() == Roi.class);
				ostream.writeObject(this.image.getOverlay().get(i));
			}
		} catch (FileNotFoundException e) {
			file.createNewFile();
			this.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void load(final File file) {
		Overlay overlay = new Overlay();
		Overlay oldOverlay = this.image.getOverlay();
		this.image.setOverlay(overlay);
		try(
			InputStream streamfile = new FileInputStream(file);
			BufferedInputStream bfile = new BufferedInputStream(streamfile);
			ObjectInputStream ostream = new ObjectInputStream(bfile);
		){
			while(true) {
				if(ostream.readBoolean()) {
					overlay.add((Roi) ostream.readObject());
				} else {
					overlay.add((OvalRoi) ostream.readObject());
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			System.out.println("cast Ex");
			throw new IllegalAccessError("not a Roi File");
		} catch (ClassNotFoundException e) {
			System.out.println("class not f");
			throw new IllegalAccessError();
		} catch (IOException e) {
			this.image.setOverlay(overlay);
			this.image.repaintOverlay();
		} catch (NullPointerException e) {
			this.exitLoader = true;
			this.image.setOverlay(oldOverlay);
			this.image.repaintOverlay();
		} finally {
			if(this.image.getOverlay().size() == 0 && !this.exitLoader) {
				JOptionPane.showMessageDialog(null, "NOT a ROI file priviusly saved. \n Retry whit another file", "Loading Error", JOptionPane.ERROR_MESSAGE);
			} else {
				this.exitLoader = false;
			}
		}
	}
}
	

