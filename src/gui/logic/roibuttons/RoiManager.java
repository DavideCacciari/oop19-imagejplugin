package gui.logic.roibuttons;

import java.awt.Point;
import java.util.function.Function;

public interface RoiManager {

	/**
	 * @param index of the ROI to be removed
	 */
	void remove(int index);

	/**
	 * @param index of the ROI 
	 * @param width to be set
	 * @param height to be set
	 */
	void changeDimension(int index, int width, int height);

	/**
	 * @param index of the ROI
	 * @param getPoint Function where the input is the old position and the output is the new one
	 */
	void move(int index, Function<Point, Point> getPoint);

}