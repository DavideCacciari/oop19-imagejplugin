package gui.logic.roibuttons;

import java.io.File;
import java.io.IOException;

public interface RoiSaverAndLoader {

	/**
	 * @param file the file where the ROIs will be saved
	 * @throws IOException
	 */
	void save(File file) throws IOException;

	/**
	 * @param file the file from where the ROIs will be load
	 */
	void load(File file);

}