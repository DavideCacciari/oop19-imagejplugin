package gui.logic.test;

import ij.ImagePlus;

public interface TestManager {

	/**
	 * @param image The reference image
	 * @return the number of TestImage
	 */
	int loadImage(ImagePlus image);

	/**
	 * remove all the test image
	 */
	void clearTest();

}