package gui.logic.test;

import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;
import ij.ImagePlus;

public class TestManagerImpl implements TestManager {
	
	private final ImagePlus refImage;
	private List<ImagePlus> testList = new LinkedList<ImagePlus>();
	
	/**
	 * @param refImage the reference image
	 */
	public TestManagerImpl(final ImagePlus refImage) {
		this.refImage = refImage;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int loadImage(final ImagePlus image) {
		if(checKDimension(image)) {
			testList.add(image);
		} else {
			String text = "The image you tried to load is not compatible with the reference image \n" +
						  "Make sure that the image is related to the reference image and retry";
			JOptionPane.showMessageDialog(null, text, "Loading Error", JOptionPane.ERROR_MESSAGE);
		}
		return this.testList.size();
	}

	private boolean checKDimension(final ImagePlus image) {
		for(int i=0; i<image.getDimensions().length; i++) {
			if(this.refImage.getDimensions()[i] != image.getDimensions()[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearTest() {
		this.testList = new LinkedList<ImagePlus>();		
	}
}
