package gui.logic.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.event.MouseInputAdapter;
import gui.logic.RoiGenerators;
import gui.logic.Shape;
import gui.view.MyGui;
import ij.gui.ImageCanvas;
import ij.gui.Overlay;

/**
 * The ImageCanvas listener, uses RoiGenerators to draw ROIs in the Overlay
 *
 */
public class ImageMouseListener extends MouseInputAdapter {
	
	private ImageCanvas refImage;
	private Overlay roi = new Overlay();
	private Shape roiShape;
	private Point firstClick;
	private boolean hasDragged = false;
	
	/**
	 * @param refImage The ImageCanvas with the reference image
	 */
	public ImageMouseListener(final ImageCanvas refImage) {
		this.refImage = refImage;
		this.roiShape = Shape.RETANGLE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		if(this.hasDragged) {
			this.roi = this.refImage.getOverlay();
			roi.add(RoiGenerators.generateRoi(this.firstClick, e.getPoint(), (Shape) this.roiShape));
			this.refImage.setOverlay(roi);
			this.refImage.repaintOverlay();
			this.hasDragged = false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		this.firstClick = e.getPoint();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		this.hasDragged = true;
		Graphics2D graph = (Graphics2D) this.refImage.getGraphics();
		graph.setColor(Color.YELLOW);
		List<Double> param = RoiGenerators.getShapeParameter(this.firstClick, e.getPoint(), (Shape) this.roiShape);
		if(this.roiShape.equals(Shape.RETANGLE)) {
			graph.clearRect(0, 0, this.refImage.getWidth(), this.refImage.getHeight());
			graph.drawImage(this.refImage.getImage().getImage(), 0, 0, this.refImage);
			this.redrawRoi(refImage.getOverlay(), graph);
			graph.drawRect(param.get(0).intValue(), param.get(1).intValue(), param.get(2).intValue(), param.get(3).intValue());
			MyGui.displaySize(param.get(2).intValue(), param.get(3).intValue());
		} else {
			graph.clearRect(0, 0, this.refImage.getWidth(), this.refImage.getHeight());
			graph.drawImage(this.refImage.getImage().getImage(), 0, 0, this.refImage);
			this.redrawRoi(refImage.getOverlay(), graph);
			graph.drawOval(param.get(0).intValue(), param.get(1).intValue() , param.get(2).intValue(), param.get(3).intValue());
			MyGui.displaySize(param.get(2).intValue(), param.get(3).intValue());
		}
		
		
	}
	
	/**
	 * Use for the purpose of debug 
	 * {@inheritDoc}
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		System.out.println(e.getPoint());
	}
	
	private void redrawRoi(Overlay overlay, Graphics2D graph) {
		for(int i=0; i<overlay.size(); i++) {
			graph.draw(overlay.get(i).getPolygon());	
		}
	}

	/**
	 * @param shape The shape for the future ROIs
	 */
	public void setShape(final Shape shape) {
		this.roiShape = shape;
	}
}
