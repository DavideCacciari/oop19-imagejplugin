package gui.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import gui.logic.RoiObserver;
import gui.logic.roibuttons.MoveKeybordListener;
import gui.logic.roibuttons.RoiManager;
import gui.logic.roibuttons.RoiManagerImpl;
import gui.logic.roibuttons.RoiSaverAndLoader;
import gui.logic.roibuttons.RoiSaverAndLoaderImpl;
import ij.gui.ImageCanvas;
import ij.gui.Overlay;

/**
 * Panel which deals with the management of the ROIs
 */
public class RoiManagmentPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private final JFileChooser chooser = new JFileChooser();
	private final RoiSaverAndLoader roiSAndL;
	private final RoiManager roiManager;
	private JTextField heightField;
	private JTextField widthField;
	private JComboBox<String> selector = new JComboBox<>();
	private JButton move;
	private JButton loadRoi;
	private ImageCanvas image;
	private JButton saveRoi;
	private JButton clear;
	private JButton removeRoi;

	/**
	 * @param refImage is the reference image
	 */
	@SuppressWarnings("deprecation")
	public RoiManagmentPanel(final ImageCanvas refImage) {
		this.image = refImage;
		this.chooser.setFileFilter(new FileNameExtensionFilter("JSON", "json"));
		this.roiSAndL = new RoiSaverAndLoaderImpl(refImage);
		this.selector.setEnabled(false);
		this.roiManager = new RoiManagerImpl(refImage.getImage());
		this.saveRoi = new JButton("Save ROI");
		this.clear = new JButton("Clear");
		this.clear.addActionListener(e -> {
			refImage.setOverlay(new Overlay());
			refImage.repaintOverlay();
			widthField.setText("");
			heightField.setText("");
		});
		this.loadRoi = new JButton("Load ROI");
		loadRoi.addActionListener(e -> {
			this.chooser.showDialog(this, "Load Roi");
			this.roiSAndL.load(chooser.getSelectedFile());
		});
		saveRoi.addActionListener(e -> {
			chooser.showSaveDialog(this);
			try {
				this.roiSAndL.save(chooser.getSelectedFile());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});
		JPanel roiButton = new JPanel();
		GridLayout roiBG =  new GridLayout(3,1);
		roiButton.setLayout(roiBG);
		roiBG.setVgap(8);
		JPanel p1 = new JPanel();
        BoxLayout layaoutP1 = new BoxLayout(p1, BoxLayout.Y_AXIS);
        p1.setLayout(layaoutP1);
        JPanel widthP = new JPanel();
        widthP.add(new JLabel("width:"));
        widthField = new JTextField(6);
        widthP.add(widthField);
        JPanel heightP = new JPanel();
        heightField = new JTextField(6);
        ActionListener textFieldL = new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				RoiManagmentPanel.this.roiManager.changeDimension(selector.getSelectedIndex(), Integer.parseInt(widthField.getText()), Integer.parseInt(heightField.getText()));
				refImage.repaintOverlay();
			}
		};
        this.heightField.addActionListener(textFieldL);
        this.widthField.addActionListener(textFieldL);
        heightP.add(new JLabel("height:"));
        heightP.add(this.heightField);
        p1.add(this.selector);
        p1.add(widthP);
        p1.add(heightP);
        this.removeRoi = new JButton("Remove");
        this.removeRoi.addActionListener(e -> {
        	this.roiManager.remove(selector.getSelectedIndex());
        	refImage.repaintOverlay();
        	if(refImage.getOverlay().size() == 0) {
        		this.widthField.setText("");
    			this.heightField.setText("");
        	}
        });
        this.selector.addPopupMenuListener(new PopupMenuListener() {
			
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {			
			}
			
			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				Rectangle rec = refImage.getOverlay().get(RoiManagmentPanel.this.selector.getSelectedIndex()).getBounds();
	        	displaySize((int) rec.getWidth(), (int) rec.getHeight());				
			}
			
			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {				
			}
		});
        p1.add(this.removeRoi);
        p1.add(Box.createVerticalStrut(3));
        this.move = new JButton(" Move ");
        p1.add(this.move);
		this.add(Box.createRigidArea(new Dimension(0, refImage.getSize().height)));
		this.add(p1);
		this.add(roiButton);
		roiButton.add(saveRoi);
		roiButton.add(loadRoi);
		roiButton.add(clear);
		RoiObserver buttonAgent = new RoiObserver(refImage.getImage(),this.selector, this.clear, this.saveRoi, this.removeRoi, this.move);
		buttonAgent.start();
		final MoveKeybordListener keyListener = new MoveKeybordListener(refImage, buttonAgent, this.roiManager);
		this.move.addActionListener(e -> {
			buttonAgent.suspend();
			MyGui.disableWhileMoving(true);
			keyListener.setMoving(true);
		});	
		this.move.addKeyListener(keyListener);
		
	}
	
	/**
	 * @param en if true set the component of the panel enable else disable all the component (beside move button) 
	 */
	public void setComponentsEnabled(final boolean en) {
		this.widthField.setEnabled(en);
		this.heightField.setEnabled(en);
		this.clear.setEnabled(en);
		this.image.setEnabled(en);
		this.selector.setEnabled(en);
		this.saveRoi.setEnabled(en);
		this.removeRoi.setEnabled(en);
		this.loadRoi.setEnabled(en);
	}
	
	/**
	 * @param width number displayed in the width field
	 * @param height number displayed in the height field
	 */
	public void displaySize(final Integer width, final Integer height) {
		this.heightField.setText(Integer.toString(height));
		this.widthField.setText(Integer.toString(width));
	}
	
	/**
	 * @return the index of the selected ROI in the Overlay
	 */
	public int getSelectedRoi() {
		if(selector.getItemCount() == 0) {
			throw new IllegalStateException();
		}
		return selector.getSelectedIndex();
	}
}
