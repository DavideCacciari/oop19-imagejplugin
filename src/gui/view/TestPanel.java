package gui.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import gui.logic.test.TestManager;
import gui.logic.test.TestManagerImpl;
import ij.ImagePlus;

/**
 * Panel which deals with the management of the Test
 *
 */
public class TestPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private TestManager manager;
	private JLabel numbersTest = new JLabel("N. of Test: 0");
	private JButton clearTest = new JButton("Clear Test");
	private JButton runTest = new JButton("Run Test");
	private List<JCheckBox> test = new ArrayList<JCheckBox>();
	private JButton loadTest;

	/**
	 * @param refImage the reference image
	 */
	public TestPanel(final ImagePlus refImage) {
		this.manager = new TestManagerImpl(refImage);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.loadTest = new JButton("Load image to test");
		JPanel testP = new JPanel();
        testP.setLayout(new BoxLayout(testP, BoxLayout.Y_AXIS));
        testP.setAlignmentX(LEFT_ALIGNMENT);
        this.add(testP);
        loadTest.setAlignmentX(RIGHT_ALIGNMENT);
        this.numbersTest.setAlignmentX(RIGHT_ALIGNMENT);
        testP.add(loadTest);
        testP.add(numbersTest);
        this.clearTest.setAlignmentX(RIGHT_ALIGNMENT);
        testP.add(Box.createRigidArea(new Dimension(0, 5)));
        testP.add(this.clearTest);
        this.clearTest.addActionListener(e -> {
        	this.numbersTest.setText("N. of Test: 0");
        	this.manager.clearTest();
        	this.runTest.setEnabled(false);
        });
        loadTest.addActionListener(e -> {
        	JFileChooser chooser = new JFileChooser();
        	chooser.showDialog(this, "Load Test Image");
        	ImagePlus image = new ImagePlus(chooser.getSelectedFile().getAbsolutePath());
        	this.numbersTest.setText("N. of Test: " + manager.loadImage(image));
        	this.runTest.setEnabled(true);
        });
        JPanel chooserP = new JPanel();
        chooserP.setLayout(new BoxLayout(chooserP, BoxLayout.Y_AXIS));
        chooserP.setAlignmentX(RIGHT_ALIGNMENT);
        JPanel checkP = new JPanel(new GridLayout(2, 3, 5, 5));
        for(int i=1; i<=6; i++) {
        	JCheckBox t = new JCheckBox("test " + i);
        	t.setBackground(Color.WHITE);
        	test.add(t);
        	checkP.add(t);
        }
        checkP.setBackground(Color.WHITE);
		checkP.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        chooserP.add(checkP);
        chooserP.add(Box.createVerticalStrut(10));
        this.runTest.setEnabled(false);
        this.runTest.addActionListener(e -> {
        	JOptionPane.showMessageDialog(null, "still NOT implemented", "Error", JOptionPane.ERROR_MESSAGE);
        });
        chooserP.add(this.runTest );
        chooserP.add(Box.createVerticalStrut(10));
        this.add(Box.createHorizontalStrut(7));
        this.add(testP);
        this.add(Box.createHorizontalGlue());
        this.add(chooserP);  
	}
	
	/**
	 * @param en if true set all the component enable else disable all the components
	 */
	public void setComponentsEnabled(final boolean en) {
		this.loadTest.setEnabled(en);
		this.runTest.setEnabled(en);
		this.clearTest.setEnabled(en);
		for(JCheckBox t : test) {
			t.setEnabled(en);
		}
	}
}
