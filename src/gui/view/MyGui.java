package gui.view;

import java.awt.BorderLayout;
import gui.logic.Shape;
import gui.logic.image.ImageMouseListener;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Toolkit;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import ij.ImagePlus;
import ij.gui.ImageCanvas;
import ij.gui.Overlay;
import ij.process.ImageProcessor;

/**
 * Is the class that displays the plug-in 
 * 
 *
 */
public class MyGui extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final String imageTitle = "Reference Image";
	private final ImageCanvas refImage;
	private final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	private final static JComboBox<Shape> shape = new JComboBox<Shape>(new Shape[] {Shape.RETANGLE, Shape.OVAL});
	private static TestPanel testPanel;
	private static RoiManagmentPanel roiManagmentPanel;
	 
    /**
     * show the GUI
     * @param arg0 is the ImageProcessor given by the plugInFilter 
     */
    public MyGui (final ImageProcessor arg0) {
    	this.setMaximumSize(screen);
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	JPanel mainP = new JPanel(new BorderLayout(2, 10));
    	this.getContentPane().add(mainP);
    	JMenuBar menu = new JMenuBar();
        menu.add(new Label("  ROI's shape: "));
        menu.add(shape);
        mainP.add(menu, BorderLayout.NORTH);
        ImagePlus refimage = new ImagePlus("riferimento", arg0);
    	this.refImage = new ImageCanvas(refimage);
    	this.refImage.setMaximumSize(new Dimension((int) (this.screen.width *0.70), (int) (this.screen.getHeight() * 0.70)));
    	refImage.setOverlay(new Overlay());
    	final ImageMouseListener mouseL = new ImageMouseListener(refImage);
    	refImage.addMouseListener(mouseL);
    	refImage.addMouseMotionListener(mouseL);
    	shape.addActionListener(e -> mouseL.setShape((Shape) shape.getSelectedItem()));
        JPanel image = new JPanel();
        TitledBorder title = BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED), imageTitle);
        title.setTitleJustification(TitledBorder.CENTER);
        image.setBorder(title);
        image.add(this.refImage);
        roiManagmentPanel = new RoiManagmentPanel(this.refImage);
        testPanel = new TestPanel(refimage);
        mainP.add(image, BorderLayout.CENTER);
        mainP.add(roiManagmentPanel, BorderLayout.EAST);
        mainP.add(testPanel, BorderLayout.SOUTH);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true); 
    }
    
    /**
     * @return the index of the selected ROI
     */
    public static int getSelectedRoi() {
    	return roiManagmentPanel.getSelectedRoi();
    }
    
    /**
     * @param b if true disable all the components of the GUI besides the move button  
     */
    public static void disableWhileMoving(boolean b) {
    	b = !b;
    	System.out.println(b);
    	roiManagmentPanel.setComponentsEnabled(b);
    	testPanel.setComponentsEnabled(b);
    	shape.setEnabled(b);
    }
    
    /**
     * @param width is the number that will be display on the width field
     * @param height is the number that will be display on the height field
     */
    public static void displaySize(final int width, final int height) {
    	roiManagmentPanel.displaySize(width, height);
    }
}
